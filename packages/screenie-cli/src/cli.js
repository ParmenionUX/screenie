import chalk from 'chalk'
import fs from 'fs'
import inquirer from 'inquirer'
import {
  discoverTestFiles,
  findTestsFromFile,
  runTests,
  addTests,
  removeNonExclusiveTestsIfSomeAreExclusive,
} from './discover-tests'
import { getProjectScreenieConfig, getAllMatrices } from './get-config'
import { buildTestsJson } from './build-json-report'
import * as status from './status'
import { startAndWaitUntilClose } from './live-browser-reporter'
import hooks from './hooks'

export default async function run(testGlobs, { filteredMatrices, outputJson, interactiveMode }) {
  const projectConfig = await getProjectScreenieConfig()
  const testMatrix = getAllMatrices(projectConfig, filteredMatrices)

  await hooks.runBefore()

  if (interactiveMode) {
    return await runInteractiveMode(projectConfig, testMatrix, testGlobs)
  }

  console.log()
  console.log(' ', 'Running tests...')
  console.log()

  const startTime = Date.now()

  const testFiles = await discoverTestFiles(testGlobs)
  const tests = testFiles.reduce(
    (currentTests, file) => [...currentTests, ...findTestsFromFile(file).tests],
    []
  )
  const testsToRun = removeNonExclusiveTestsIfSomeAreExclusive(tests)
  await runTests(testsToRun, testMatrix)

  const endTime = Date.now()

  const failedTests = status.getFailedTests()
  const totalPassed = status.getTotalPassed()
  const totalFailed = status.getTotalFailed()

  if (failedTests.length > 0 && process.stdin.isTTY) {
    console.log()

    // write JSON before prompting for diffs
    if (outputJson) {
      fs.writeFileSync(outputJson, JSON.stringify(buildTestsJson(), null, 2))
    }

    const { openDiffs } = await inquirer.prompt([
      {
        type: 'confirm',
        message:
          'Some tests had failures. Do you want to open a browser to see the diffs?',
        name: 'openDiffs',
      },
    ])

    if (openDiffs) {
      await startAndWaitUntilClose()
    }
  }

  await hooks.runAfter()

  console.log()

  if (failedTests.length === 0) {
    // no failed tests
    console.log(' ', chalk.green('Finished running tests, all passed'))
  } else {
    // some failed tests
    console.log(' ', chalk.red('Finished running tests with failures'))
  }

  console.log()

  console.log(' ', 'Passed      ', totalPassed)
  console.log(' ', 'Failed      ', totalFailed)
  console.log(' ', 'Time taken  ', ((endTime - startTime) / 1000).toFixed(2) + 's')

  console.log()

  if (outputJson) {
    fs.writeFileSync(outputJson, JSON.stringify(buildTestsJson(), null, 2))
  }

  if (failedTests.length > 0) {
    process.exit(1)
  } else {
    process.exit()
  }
}

async function runInteractiveMode(projectConfig, testMatrix, testGlobs) {
  const testFiles = await discoverTestFiles(testGlobs)
  const tests = testFiles.reduce(
    (currentTests, file) => [...currentTests, ...findTestsFromFile(file).tests],
    []
  )
  const testsToRun = removeNonExclusiveTestsIfSomeAreExclusive(tests)
  await addTests(testsToRun, testMatrix)

  await startAndWaitUntilClose()

  await hooks.runAfter()

  console.log()
  console.log(' ', 'Exited interactive mode')
  console.log()

  process.exit()
}
