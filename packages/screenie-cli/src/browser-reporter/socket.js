import io from 'socket.io-client'
import store from './store'
import * as actions from './actions'

const socket = io('/')

socket.on('disconnect', () => window.close())

socket.on('status', status => {
  console.log('Got status', status)

  store.dispatch(actions.statusReceived(status))
})

export default socket
