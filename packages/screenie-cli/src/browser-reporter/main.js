import React from 'react'
import { Header } from 'semantic-ui-react'

import TestTable from './components/test-table'

export default () =>
  <div>
    <Header as="h1">Test Results</Header>

    <TestTable />
  </div>
