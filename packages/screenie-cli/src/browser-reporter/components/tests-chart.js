import React from 'react'
import { connect } from 'react-redux'

export default connect(
  state => ({
    status: state.status,
  }),
)(({ status }) =>
  <div style={styles.container}>
    <svg style={styles.svg}>
      {renderTests(status.tests)}
    </svg>
  </div>
)

const PADDING = 20
const TEST_PADDING = 30
const SNAPSHOT_PADDING = 5
const SNAPSHOT_SIZE = 16

function renderTests(tests) {
  let x = PADDING

  return tests.map(test => {
    const startX = x

    if (test.snapshots != null) {
      const snapshots = test.snapshots.map(snapshot => {
        const thisX = x
        x += SNAPSHOT_SIZE + SNAPSHOT_PADDING

        return renderSnapshotOrTest(snapshot, test, thisX)
      })

      return [
        <rect x={startX} width={x - startX} y={0} height={SNAPSHOT_SIZE + PADDING + PADDING} fill="whitesmoke" />,
        ...snapshots,
        <line
          x1={x + SNAPSHOT_SIZE + (TEST_PADDING / 2)}
          x2={x + SNAPSHOT_SIZE + (TEST_PADDING / 2)}
          y1={PADDING} y2={PADDING + SNAPSHOT_SIZE} stroke="gray" strokeWidth={1}
        />,
      ]
    }

    const thisX = x
    x += SNAPSHOT_SIZE + TEST_PADDING

    return [
      renderSnapshotOrTest(test, null, thisX),
      <line
        x1={thisX + SNAPSHOT_SIZE + (TEST_PADDING / 2)}
        x2={thisX + SNAPSHOT_SIZE + (TEST_PADDING / 2)}
        y1={PADDING} y2={PADDING + SNAPSHOT_SIZE} stroke="gray" strokeWidth={1}
      />,
    ]
  })
}

function renderSnapshotOrTest(testOrSnapshot, parentTest, x) {
  let snapshotStatusStyle = null

  if (testOrSnapshot.passed) {
    snapshotStatusStyle = styles.snapshotPassed
  } else if (testOrSnapshot.passed === false) {
    snapshotStatusStyle = styles.snapshotFailed
  }

  return (
    <rect
      style={{
        ...styles.snapshot,
        ...snapshotStatusStyle,
      }}
      x={x}
      y={PADDING}
    >
      <title>
        {parentTest != null
          ? `${parentTest.name} - ${testOrSnapshot.name}`
          : testOrSnapshot.name
        }
      </title>
    </rect>
  )
}

const styles = {
  container: {
    width: '100%',
  },
  svg: {
    width: '100%',
    height: 300,
  },
  snapshot: {
    fill: 'gray',
    width: SNAPSHOT_SIZE,
    height: SNAPSHOT_SIZE,
  },
  snapshotPassed: {
    fill: 'green',
  },
  snapshotFailed: {
    fill: 'red',
  },
}
