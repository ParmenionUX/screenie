import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Icon, Table, Button } from 'semantic-ui-react'

import socket from '../socket'

const rerunTest = testId => socket.emit('rerunAllMatrices', { testId })

export default connect(state => ({
  status: state.status,
}))(({ status }) =>
  <Table celled structured selectable>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell rowSpan="2">Name</Table.HeaderCell>
        <Table.HeaderCell rowSpan="2">Overall Result</Table.HeaderCell>
        <Table.HeaderCell colSpan={status.matrices.length}>Matrix</Table.HeaderCell>
        <Table.HeaderCell rowSpan="2">Actions</Table.HeaderCell>
      </Table.Row>
      <Table.Row>
        {status.matrices.map(matrix =>
          <Table.HeaderCell key={matrix} textAlign="center" style={{ width: 100 }}>
            {matrix}
          </Table.HeaderCell>
        )}
      </Table.Row>
    </Table.Header>

    <Table.Body>
      {status.tests.map(test =>
        <Table.Row key={test.id}>
          <Table.Cell selectable>
            <Link to={`/test/${test.id}`}>
              {test.name}

              {test.snapshots != null &&
                test.snapshots.map(snapshot =>
                  <div style={{ marginLeft: 20, fontSize: 11 }}>
                    {snapshot}
                  </div>
                )}
            </Link>
          </Table.Cell>
          {renderPassedCell(test)}
          {renderTestMatrixCells(test, status.matrices)}
          <Table.Cell>
            <Button compact size="mini" as={Link} to={`/test/${test.id}`}>
              View
            </Button>
            <Button
              basic
              compact
              size="mini"
              onClick={() => rerunTest(test.id)}
            >
              Rerun test
            </Button>
          </Table.Cell>
        </Table.Row>
      )}
    </Table.Body>
  </Table>
)

const renderPassedCell = test => {
  if (test.passed === true) {
    return (
      <Table.Cell textAlign="center" positive>
        <Icon color="green" name="checkmark" size="large" /> Passed
      </Table.Cell>
    )
  }

  if (test.passed == null) {
    return (
      <Table.Cell textAlign="center">
        <Icon color="grey" name="dot circle outline" size="large" /> Pending
      </Table.Cell>
    )
  }

  return (
    <Table.Cell textAlign="center" negative>
      <Icon color="red" name="close" size="large" /> Failed
    </Table.Cell>
  )
}

const renderTestMatrixCells = (test, matrixLabels) =>
  matrixLabels.map(matrixLabel => {
    const matrixForTest = test.matrices.find(_matrix => _matrix.label === matrixLabel)

    if (!matrixForTest) {
      return (
        <Table.Cell disabled textAlign="center">
          -
        </Table.Cell>
      )
    }

    let icon = null

    if (matrixForTest.passed) {
      icon = <Icon color="green" name="checkmark" size="large" />
    } else if (matrixForTest.passed == null) {
      icon = <Icon color="grey" name="dot circle outline" size="large" />
    } else {
      icon = <Icon color="red" name="close" size="large" />
    }

    return (
      <Table.Cell
        textAlign="center"
        positive={matrixForTest.passed}
        negative={matrixForTest.passed === false}
      >
        {icon}

        {matrixForTest.snapshots != null &&
          matrixForTest.snapshots.map(snapshot =>
            <div>
              <SnapshotStatusIcon passed={snapshot.passed} />
            </div>
          )}
      </Table.Cell>
    )
  })

const SnapshotStatusIcon = ({ passed }) => {
  if (passed === true) {
    return <Icon color="green" name="checkmark" size="small" />
  } else if (passed == null) {
    return <Icon color="grey" name="circle" size="small" />
  } else {
    return <Icon color="red" name="close" size="small" />
  }
}
