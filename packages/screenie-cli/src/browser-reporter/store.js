import { createStore, applyMiddleware } from 'redux'
import { handleActions } from 'redux-actions'
import logger from 'redux-logger'
import * as actions from './actions'

const INITIAL_STATE = {
  status: {
    tests: [],
    matrices: [],
  },
}

const reducer = handleActions(
  {
    [actions.statusReceived]: (state, action) => ({
      ...state,
      status: {
        tests: action.payload,
        matrices: getMatricesFromTests(action.payload),
      },
      selectedMatrix: state.selectedMatrix || getMatricesFromTests(action.payload)[0],
    }),
    [actions.selectMatrix]: (state, action) => ({
      ...state,
      selectedMatrix: action.payload,
    }),
  },
  INITIAL_STATE
)

export default createStore(reducer, applyMiddleware(logger))

function getMatricesFromTests(tests) {
  const matrices = []

  for (const test of tests) {
    for (const matrix of test.matrices) {
      if (!matrices.includes(matrix.label)) {
        matrices.push(matrix.label)
      }
    }
  }

  return matrices
}
