import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Header, Menu, Label, Message, Grid, Button } from 'semantic-ui-react'

import * as actions from './actions'
import socket from './socket'

const rerunTest = (testId, matrixId) => socket.emit('rerun', { testId, matrixId })
const rerunAllMatrices = testId => socket.emit('rerunAllMatrices', { testId })

const updateReferenceScreenshot = (testId, matrixId, snapshot) =>
  socket.emit('updateReference', { testId, matrixId, snapshot })
const updateAllReferenceScreenshotsForSnapshots = (testId, matrixId) =>
  socket.emit('updateReferenceForAllSnapshots', { testId, matrixId })

export default connect(
  (state, props) => ({
    test: state.status.tests.find(_test => _test.id === props.match.params.testId),
    selectedMatrix: state.selectedMatrix,
  }),
  dispatch => ({
    selectMatrix: matrix => dispatch(actions.selectMatrix(matrix)),
  })
)(
  ({ test, selectedMatrix, selectMatrix, match }) =>
    test == null
      ? <div>Test not found</div>
      : <div>
        <Header as="h1">
          {test.name}

          <StatusDot passed={test.passed} />
        </Header>

        <p>
          <Button basic as={Link} to="/">
              Back to test overview
            </Button>{' '}
          <Button onClick={() => rerunAllMatrices(test.id)}>Rerun this test</Button>
        </p>

        <p>
          {test.filePath}
        </p>

        <Menu pointing secondary>
          {test.matrices.map(matrix =>
            <Menu.Item
              key={matrix.id}
              active={matrix.label === selectedMatrix}
              onClick={selectMatrix.bind(null, matrix.label)}
              >
              {matrix.label}

              <StatusDot passed={matrix.passed} />
            </Menu.Item>
            )}
        </Menu>

        <MatrixDetails
          key={selectedMatrix}
          matrix={test.matrices.find(_matrix => _matrix.label === selectedMatrix)}
          test={test}
          />
      </div>
)

const MatrixDetails = ({ matrix, test }) => {
  let status =
    matrix.passed === true
      ? <Message positive size="large">
        <Message.Header>
            This test has passed and matched all of the reference screenshots
          </Message.Header>

        <br />

        <Button onClick={() => rerunTest(test.id, matrix.id)}>Re-run test</Button>
      </Message>
      : <Message negative size="large">
        <Message.Header>This test has failed</Message.Header>
        <p>Please inspect the diffs below and take appropriate action</p>
        <Button onClick={() => rerunTest(test.id, matrix.id)} primary>
            Re-run test
          </Button>{' '}
        {matrix.snapshots != null
            ? <Button
              color="yellow"
              onClick={() =>
                  updateAllReferenceScreenshotsForSnapshots(test.id, matrix.id)}
              >
                Update reference screenshots for all snapshots
              </Button>
            : <Button
              color="yellow"
              onClick={() =>
                  updateAllReferenceScreenshotsForSnapshots(test.id, matrix.id)}
              >
                Update reference screenshot
              </Button>}
      </Message>

  if (matrix.passed == null) {
    status = (
      <Message size="large">
        <Message.Header>This test is pending and is running now</Message.Header>
      </Message>
    )
  }

  return (
    <div>
      {status}

      {matrix.snapshots != null
        ? <div>
          {matrix.snapshots.map(snapshot =>
            <SnapshotDetails
              key={snapshot.name}
              matrix={matrix}
              test={test}
              snapshot={snapshot}
              snapshotName={snapshot.name}
              passed={snapshot.passed}
              />
            )}
        </div>
        : <SnapshotDetails matrix={matrix} test={test} passed={matrix.passed} />}
    </div>
  )
}

const SnapshotDetails = ({ matrix, test, snapshot, snapshotName, passed }) =>
  <div>
    {snapshot != null &&
      <Header as="h4">
        {snapshot.name}
        <StatusDot passed={passed} /> {' '}
        {passed === false &&
          <Button
            basic
            color="yellow"
            size="tiny"
            compact
            onClick={() => updateReferenceScreenshot(test.id, matrix.id, snapshotName)}
          >
            UPDATE
          </Button>}
      </Header>}

    {passed !== false
      ? <Grid>
        <Grid.Row columns={2}>
          <Grid.Column>
            <div style={{ position: 'relative' }}>
              <img
                src={`/screenshots/${test.id}/${matrix.id}/reference?snapshot=${snapshotName}&rnd=${Math.random()}`}
                style={{ opacity: passed != null ? 1 : 0.3, maxWidth: '100%' }}
                />

              {passed == null &&
              <div
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  fontWeight: 'bold',
                  fontSize: 21,
                }}
                  >
                    This test has not run yet
                  </div>}
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      : <Grid>
        <Grid.Row columns={2}>
          <Grid.Column>
            <Header as="h5">Overlapped screenshots</Header>

            <div style={{ position: 'relative' }}>
              <img
                src={`/screenshots/${test.id}/${matrix.id}/reference?snapshot=${snapshotName}&rnd=${Math.random()}`}
                style={{ position: 'absolute', top: 0, left: 0, maxWidth: '100%' }}
                />
              <img
                src={`/screenshots/${test.id}/${matrix.id}/new?snapshot=${snapshotName}&rnd=${Math.random()}`}
                style={{ opacity: 0.5, maxWidth: '100%' }}
                />
            </div>
          </Grid.Column>
          <Grid.Column>
            <Header as="h5">Pdiff</Header>

            <img
              src={`/screenshots/${test.id}/${matrix.id}/diff?snapshot=${snapshotName}&rnd=${Math.random()}`}
              style={{ maxWidth: '100%' }}
              />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={2}>
          <Grid.Column>
            <Header as="h5">Reference</Header>

            <img
              src={`/screenshots/${test.id}/${matrix.id}/reference?snapshot=${snapshotName}&rnd=${Math.random()}`}
              style={{ maxWidth: '100%' }}
              />
          </Grid.Column>
          <Grid.Column>
            <Header as="h5">New</Header>

            <img
              src={`/screenshots/${test.id}/${matrix.id}/new?snapshot=${snapshotName}&rnd=${Math.random()}`}
              style={{ maxWidth: '100%' }}
              />
          </Grid.Column>
        </Grid.Row>
      </Grid>}
  </div>

const StatusDot = ({ passed }) => {
  if (passed === true) {
    return <Label circular color="green" empty />
  } else if (passed == null) {
    return <Label circular color="grey" empty />
  } else {
    return <Label circular color="red" empty />
  }
}
