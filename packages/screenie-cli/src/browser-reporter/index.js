import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Container } from 'semantic-ui-react'
import { HashRouter as Router, Route } from 'react-router-dom'
import 'semantic-ui-css/semantic.min.css'

import store from './store'
import './socket'

import Main from './main'
import TestDetails from './test-details'

const App = () =>
  <Provider store={store}>
    <Router>
      <Container fluid>
        <Route exact path="/" component={Main} />
        <Route path="/test/:testId" component={TestDetails} />
      </Container>
    </Router>
  </Provider>

ReactDOM.render(<App />, document.getElementById('container'))
