import { createAction } from 'redux-actions'

export const statusReceived = createAction('STATUS_RECEIVED')
export const selectMatrix = createAction('SELECT_MATRIX')
