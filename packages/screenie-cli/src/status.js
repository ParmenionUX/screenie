import uuid from 'uuid/v4'
import { EventEmitter } from 'events'
import * as log from './logger'
import { getReferenceScreenshotPath } from './util'

const tests = []
let totalPassed = 0
let totalFailed = 0

const TEST_DEFINITION_SYMBOL = Symbol('test')

export const events = new EventEmitter()

export function addTest(test, matrix, matrixLabel) {
  let testStatus = tests.find(
    _test => _test.name === test.name && _test.filePath === test.filePath
  )

  if (!testStatus) {
    testStatus = {
      id: uuid(),
      name: test.name,
      filePath: test.filePath,
      snapshots: test.snapshots.length === 0 ? null : test.snapshots,
      matrices: [],
    }

    testStatus[TEST_DEFINITION_SYMBOL] = test

    tests.push(testStatus)
  }

  let matrixStatus = testStatus.matrices.find(
    _matrixStatus => _matrixStatus.label === matrixLabel
  )

  if (!matrixStatus) {
    matrixStatus = {
      id: uuid(),
      label: matrixLabel,
      matrix,
      snapshots:
        test.snapshots.length === 0
          ? null
          : test.snapshots.map(snapshotName => ({
            name: snapshotName,
            referenceScreenshotPath: getReferenceScreenshotPath(
                test.filePath,
                test,
                snapshotName,
                matrix
              ),
          })),
    }

    if (test.snapshots.length === 0) {
      matrixStatus.referenceScreenshotPath = getReferenceScreenshotPath(
        test.filePath,
        test,
        null,
        matrix
      )
    }

    testStatus.matrices.push(matrixStatus)
  }

  events.emit('change')
}

export function logTestRun(test, matrixLabel) {
  const { testStatus, matrixStatus } = getTestAndMatrixStatus(test, matrixLabel)

  if (!matrixStatus || !testStatus) return

  matrixStatus.passed = null
  testStatus.passed = null

  delete matrixStatus.error
  delete matrixStatus.isNew
  delete matrixStatus.isMismatch

  if (matrixStatus.snapshots) {
    for (const snapshot of matrixStatus.snapshots) {
      snapshot.passed = null

      delete snapshot.error
      delete snapshot.isNew
      delete snapshot.isMismatch
    }
  }

  log.run(test.name, matrixLabel)

  events.emit('change')
}

export function logTestSuccess(test, matrixLabel, { isNew = false } = {}) {
  const { testStatus, matrixStatus } = getTestAndMatrixStatus(test, matrixLabel)

  if (!matrixStatus || !testStatus) return

  matrixStatus.passed = true
  matrixStatus.isNew = isNew

  if (
    testStatus.matrices.filter(_matrix => _matrix.passed === true).length ===
    testStatus.matrices.length
  ) {
    testStatus.passed = true
  }

  if (isNew) {
    log.newCapture(test.name, matrixLabel)
  } else {
    log.success(test.name, matrixLabel)
  }

  totalPassed++

  events.emit('change')
}

export function logTestFail(
  test,
  matrixLabel,
  { message = null, isMismatch = false, exception } = {}
) {
  const { testStatus, matrixStatus } = getTestAndMatrixStatus(test, matrixLabel)

  if (!matrixStatus || !testStatus) return

  testStatus.passed = false

  matrixStatus.passed = false
  matrixStatus.error = message
  matrixStatus.isMismatch = isMismatch

  if (isMismatch && exception.exportedPaths != null) {
    matrixStatus.exportedPaths = exception.exportedPaths
  }

  if (isMismatch) {
    log.mismatch(test.name, matrixLabel)
  } else {
    log.fail(test.name, matrixLabel)

    console.log()
    console.log('Exception', exception)
    console.log(exception.stack || exception)
  }

  totalFailed++

  events.emit('change')
}

export function logSnapshotSuccess(test, matrixLabel, snapshot, { isNew = false } = {}) {
  const { snapshotStatus } = getTestMatrixSnapshotStatus(test, matrixLabel, snapshot)

  if (!snapshotStatus) return

  snapshotStatus.passed = true
  snapshotStatus.isNew = isNew

  events.emit('change')
}

export function logSnapshotFail(
  test,
  matrixLabel,
  snapshot,
  { message = null, exception = null, isMismatch = false } = {}
) {
  const { snapshotStatus } = getTestMatrixSnapshotStatus(test, matrixLabel, snapshot)

  if (!snapshotStatus) return

  snapshotStatus.passed = false
  snapshotStatus.error = message
  snapshotStatus.isMismatch = isMismatch

  if (isMismatch && exception.exportedPaths != null) {
    snapshotStatus.exportedPaths = exception.exportedPaths
  }

  events.emit('change')
}

export const getTests = () => tests

export const getPassedTests = () => tests.filter(test => test.passed === true)
export const getFailedTests = () => tests.filter(test => test.passed === false)

export const getTestById = id => tests.find(_test => _test.id === id)

export const getTotalPassed = () => totalPassed
export const getTotalFailed = () => totalFailed

export const getTestAndMatrixByIds = (testId, matrixId, snapshotName = null) => {
  const test = getTestById(testId)

  if (!test) {
    return { test: null, matrix: null }
  }

  const matrix = test.matrices.find(_matrix => _matrix.id === matrixId)
  let snapshot

  if (matrix.snapshots != null && snapshotName != null) {
    snapshot = matrix.snapshots.find(_snapshot => _snapshot.name === snapshotName)
  }

  return {
    test,
    matrix,
    snapshot,
  }
}

export const getTestDefinitionById = testId => {
  const test = tests.find(_test => _test.id === testId)

  if (!test) return null

  return test[TEST_DEFINITION_SYMBOL]
}

export const getMatrixDefinitionById = (testId, matrixId) => {
  const test = tests.find(_test => _test.id === testId)

  if (!test) return null

  const matrix = test.matrices.find(_matrix => _matrix.id === matrixId)

  return matrix.matrix
}

const getTestAndMatrixStatus = (test, matrixLabel) => {
  const testStatus = tests.find(
    _test => _test.name === test.name && _test.filePath === test.filePath
  )

  if (!testStatus) return { testStatus: null, matrixStatus: null }

  const matrixStatus = testStatus.matrices.find(
    _matrixStatus => _matrixStatus.label === matrixLabel
  )

  return {
    testStatus,
    matrixStatus,
  }
}

const getTestMatrixSnapshotStatus = (test, matrixLabel, snapshot) => {
  const { testStatus, matrixStatus } = getTestAndMatrixStatus(test, matrixLabel)

  if (!matrixStatus) {
    return { testStatus, matrixStatus, snapshotStatus: null }
  }

  const snapshotStatus = matrixStatus.snapshots.find(
    _snapshotStatus => _snapshotStatus.name === snapshot
  )

  return {
    testStatus,
    matrixStatus,
    snapshotStatus,
  }
}
