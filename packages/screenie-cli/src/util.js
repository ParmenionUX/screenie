import _ from 'lodash'
import path from 'path'
import speakingurl from 'speakingurl'

const slug = string => speakingurl(string, { symbols: false })

const getSnapshotNameSlug = snapshotName =>
  snapshotName != null ? '--' + slug(snapshotName) : ''

const getMatrixNameSlug = matrix => slug(_.values(matrix).join('-'))

export function getReferenceScreenshotPath(filePath, test, snapshotName, matrix) {
  const matrixNameSlug = getMatrixNameSlug(matrix)
  const fileName = path.basename(filePath, path.extname(filePath))

  if (snapshotName != null) {
    return path.join(
      path.dirname(filePath),
      '__screenshots__',
      `${fileName}-${slug(test.name)}`,
      slug(snapshotName),
      `${matrixNameSlug}.png`
    )
  }

  return path.join(
    path.dirname(filePath),
    '__screenshots__',
    `${fileName}-${slug(test.name)}`,
    `${matrixNameSlug}.png`
  )
}

export function getDiffOutputPath(filePath, test, snapshotName, matrix) {
  const snapshotNameSlug = getSnapshotNameSlug(snapshotName)
  const matrixNameSlug = getMatrixNameSlug(matrix)

  const diffOutputPath = path.join(
    process.cwd(),
    'diff_output',
    `${path.basename(filePath, path.extname(filePath))}-${slug(
      test.name
    )}${snapshotNameSlug}`,
    matrixNameSlug
  )

  return diffOutputPath
}
