import path from 'path'
import _ from 'lodash'
import exists from 'path-exists'

export async function getProjectScreenieConfig() {
  const configPath = path.join(process.cwd(), 'screenie.config.js')

  if (!await exists(configPath)) {
    return {}
  }

  const config = require(configPath)

  return config || {}
}

export function getAllMatrices(projectConfig, filteredMatricesIn) {
  const { matrices = {}, onlyRun = {} } = projectConfig

  const filteredMatrices =
    filteredMatricesIn == null
      ? matrices
      : {
        ...matrices,
        ..._.pick(filteredMatricesIn, Object.keys(matrices)),
      }

  const matrixNames = Object.keys(filteredMatrices)

  if (matrixNames.length === 0) {
    return [{ matrix: 'default' }]
  }

  if (matrixNames.length === 0) {
    throw new Error('No matrices defined or provided')
  }

  return flattenMatrices(0, matrixNames, filteredMatrices, onlyRun)
}

function isAllowedToRunMatrixElement(element, onlyRun, current, currentMatrixName) {
  if (onlyRun != null) {
    const keysSoFar = Object.keys(current)

    for (const key of keysSoFar) {
      const value = current[key]

      if (onlyRun[key] != null && onlyRun[key][value] != null && onlyRun[key][value][currentMatrixName] != null) {
        const allowedMatrices = onlyRun[key][value][currentMatrixName]

        if (!allowedMatrices.includes(element)) {
          return false
        }
      }
    }
  }

  return true
}

function flattenMatrices(currentMatrixNameIndex, matrixNames, matrices, onlyRun, current = {}) {
  const currentMatrixName = matrixNames[currentMatrixNameIndex]
  const currentMatrix = matrices[currentMatrixName]
  const nextMatrixNameIndex = currentMatrixNameIndex + 1
  const result = []

  for (const element of currentMatrix) {
    if (!isAllowedToRunMatrixElement(element, onlyRun, current, currentMatrixName)) continue

    if (nextMatrixNameIndex < matrixNames.length) {
      result.push(
        ...flattenMatrices(
          nextMatrixNameIndex,
          matrixNames,
          matrices,
          onlyRun,
          { ...current, [currentMatrixName]: element },
        ).map(nextConfig => ({
          [currentMatrixName]: element,
          ...nextConfig,
        }))
      )
    } else {
      result.push({
        [currentMatrixName]: element,
      })
    }
  }

  return result
}
