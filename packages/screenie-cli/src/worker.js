import workerpool from 'workerpool'
import * as screenie from 'screenie-core'

workerpool.worker({
  async isDifferent(lastScreenshotPath, tmpScreenshotPath) {
    return await screenie.isDifferent(lastScreenshotPath, tmpScreenshotPath)
  },

  async matchScreenshot(
    testDefinitionName,
    referenceScreenshotPath,
    finalScreenshotPath,
    diffOutputPath
  ) {
    return await screenie.matchScreenshot(
      testDefinitionName,
      referenceScreenshotPath,
      finalScreenshotPath,
      diffOutputPath
    )
  },
})
