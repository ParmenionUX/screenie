import * as status from './status'

export function buildTestsJson() {
  return status.getTests()
}
