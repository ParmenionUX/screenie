import chalk from 'chalk'

const MATRIX_NAME_LENGTH = 32

export const run = (test, matrixName = '') =>
  console.log(
    '  ' + chalk.white.bgBlue.bold(' RUNNING '),
    chalk.gray(matrixName.substring(0, MATRIX_NAME_LENGTH).padEnd(MATRIX_NAME_LENGTH)),
    chalk.white.bold(test)
  )

export const fail = (test, matrixName = '') =>
  console.log(
    '  ' + chalk.white.bgRed.bold(' FAILURE '),
    chalk.gray(matrixName.substring(0, MATRIX_NAME_LENGTH).padEnd(MATRIX_NAME_LENGTH)),
    chalk.white.bold(test)
  )

export const mismatch = (test, matrixName = '') =>
  console.log(
    '  ' + chalk.white.bgRed.bold(' CHANGED '),
    chalk.gray(matrixName.substring(0, MATRIX_NAME_LENGTH).padEnd(MATRIX_NAME_LENGTH)),
    chalk.white.bold(test)
  )

export const success = (test, matrixName = '') =>
  console.log(
    '  ' + chalk.white.bgGreen.bold(' SUCCESS '),
    chalk.gray(matrixName.substring(0, MATRIX_NAME_LENGTH).padEnd(MATRIX_NAME_LENGTH)),
    chalk.white.bold(test)
  )

export const newCapture = (test, matrixName = '') =>
  console.log(
    '  ' + chalk.white.bgYellow.bold('   NEW   '),
    chalk.gray(matrixName.substring(0, MATRIX_NAME_LENGTH).padEnd(MATRIX_NAME_LENGTH)),
    chalk.white.bold(test)
  )
