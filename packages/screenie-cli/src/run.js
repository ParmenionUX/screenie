import proxyquire from 'proxyquire'
import StateMachine from 'fsm-as-promised'
import { EventEmitter } from 'events'
import { captureWithRetries } from './capture'

const TEST_STATE_MACHINE = [
  { name: 'start', from: 'queued', to: 'pending' }, // TODO this needs to be lifted out to the manager?
  { name: 'prepare', from: 'pending', to: 'preparing' },
  { name: 'prepareCapture', from: 'preparing', to: 'preparingCapture' },
  { name: 'capture', from: 'preparingCapture', to: 'capturing' },
  { name: 'prepareCapture', from: 'capturing', to: 'preparingCapture' },
  { name: 'capture', from: 'capturing', to: 'capturing' },
  { name: 'exit', from: 'capturing', to: 'exiting' },
  { name: 'exit', from: 'preparingCapture', to: 'exiting' },
  { name: 'end', from: 'exiting', to: 'exited' },
]

function getTestStateTransitions(definition, matrix) {
  const testDefinitions = []

  const testFn = (name, definition) => testDefinitions.push({ name, definition })
  testFn.only = (name, definition) => testDefinitions.push({ name, definition })

  const stubbedModules = {
    'screenie-cli': {
      test: testFn,
    },
  }

  try {
    proxyquire(definition.filePath, stubbedModules)
  } catch (ex) {
    console.error(ex.stack || ex.message)

    throw new Error('Error requiring test file ' + definition.filePath)
  }

  return testDefinitions.find(def => def.name === definition.name).definition.run(matrix)
}

export async function runTestDefinition(testDefinition, matrix, eventsCallback) {
  const events = new EventEmitter()
  eventsCallback(events)

  const stateTransitions = getTestStateTransitions(testDefinition, matrix)

  const fsm = StateMachine({
    initial: 'queued',
    events: TEST_STATE_MACHINE,
    callbacks: {
      ...stateTransitions,

      onenter(options) {
        events.emit('worker.state', {
          from: options.from,
          to: options.to,
        })
      },
    },
    error: (msg, options) => {
      console.log('Got ERROR', msg, options)
      throw new Error('Got state error', msg)
    },
  })

  await fsm.start()
  await fsm.prepare()

  let hasNewCapture = false

  try {
    if (testDefinition.snapshots.length > 0) {
      let firstException = null

      for (const snapshot of testDefinition.snapshots) {
        try {
          await fsm.prepareCapture(snapshot)

          const res = await captureWithRetries(
            testDefinition,
            matrix,
            fsm.capture,
            testDefinition.filePath,
            snapshot
          )

          events.emit('worker.snapshotPass', {
            snapshot,
            isNew: res.isNew,
          })

          if (res.isNew) {
            hasNewCapture = true
          }
        } catch (ex) {
          const isMismatch = ex.exportedPaths != null

          events.emit('worker.snapshotFail', {
            snapshot,
            isMismatch,
            exception: ex,
            message: ex.message,
          })

          if (isMismatch) {
            // keep running test so all snapshots get captured even if there is a failure.
            // save the exception for later so we can still throw it
            if (firstException == null) {
              firstException = ex
            }
          } else {
            throw ex
          }
        }
      }

      if (firstException != null) {
        throw firstException
      }
    } else {
      await fsm.prepareCapture(null)

      const res = await captureWithRetries(
        testDefinition,
        matrix,
        fsm.capture,
        testDefinition.filePath,
        null
      )

      if (res.isNew) {
        hasNewCapture = true
      }
    }
  } catch (ex) {
    await fsm.exit()
    await fsm.end()

    throw ex
  }

  await fsm.exit()
  await fsm.end()

  return {
    isNew: hasNewCapture,
  }
}
