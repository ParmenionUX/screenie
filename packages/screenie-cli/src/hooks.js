const before = []
const after = []

async function runBefore() {
  for (const beforeHook of before) {
    await beforeHook()
  }
}

async function runAfter() {
  for (const afterHook of after) {
    await afterHook()
  }
}

export default {
  before,
  after,

  runBefore,
  runAfter,
}
