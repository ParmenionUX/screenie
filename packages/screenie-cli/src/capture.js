import invariant from 'invariant'
import exists from 'path-exists'
import workerpool from 'workerpool'
import path from 'path'

const diffPool = workerpool.pool(path.join(__dirname, './worker.js'))

import { getReferenceScreenshotPath, getDiffOutputPath } from './util'

const debug = require('debug')('screenie-cli:capture')

const CAPTURE_ATTEMPTS = 5
const CAPTURE_DELAY_BETWEEN_ATTEMPTS = 150

export async function capture(testDefinition, matrix, captureFn, testFile, snapshotName) {
  const referenceScreenshotPath = getReferenceScreenshotPath(
    testFile,
    testDefinition,
    snapshotName,
    matrix
  )

  const diffOutputPath = getDiffOutputPath(testFile, testDefinition, snapshotName, matrix)

  const isNew = !await exists(referenceScreenshotPath)

  if (isNew) {
    debug(
      'Got test without reference screenshot, taking several new captures for %s',
      testDefinition.name
    )

    let lastScreenshotPath = null
    let finalScreenshotPath = null

    for (let i = 0; i < CAPTURE_ATTEMPTS; i++) {
      const delay = CAPTURE_DELAY_BETWEEN_ATTEMPTS * 2 ** i // exponential back off for delay
      await new Promise(resolve => setTimeout(resolve, delay))

      debug('Capture attempt %s for NEW screenshot %s', i, testDefinition.name)

      const tmpScreenshotPath = await captureFn(snapshotName)
      invariant(!!tmpScreenshotPath, 'capture(): No screenshot path returned')

      // is different from last screenshot taken?
      if (!lastScreenshotPath) {
        lastScreenshotPath = tmpScreenshotPath
        continue
      }

      if (await diffPool.exec('isDifferent', [lastScreenshotPath, tmpScreenshotPath])) {
        debug('Screenshot was different, taking another one', testDefinition.name)
        lastScreenshotPath = tmpScreenshotPath
        continue
      }

      // no difference, keep this screenshot!
      debug(
        'Got consistent screenshot for test %s after %s captures',
        testDefinition.name,
        i + 1
      )

      lastScreenshotPath = null
      finalScreenshotPath = tmpScreenshotPath
      break
    }

    if (!finalScreenshotPath) {
      throw new Error("Couldn't take a consistent first capture")
    }

    return await diffPool.exec('matchScreenshot', [
      testDefinition.name,
      referenceScreenshotPath,
      finalScreenshotPath,
      diffOutputPath,
    ])
  }

  const tmpScreenshotPath = await captureFn(snapshotName)
  invariant(!!tmpScreenshotPath, 'capture(): No screenshot path returned')

  debug('Matching screenshot for test %s', testDefinition.name)

  try {
    return await diffPool.exec('matchScreenshot', [
      testDefinition.name,
      referenceScreenshotPath,
      tmpScreenshotPath,
      diffOutputPath,
    ])
  } catch (ex) {
    throw ex
  }
}

export async function captureWithRetries(
  testDefinition,
  matrix,
  captureFn,
  testFile,
  snapshotName
) {
  let exception = null

  for (let i = 0; i < CAPTURE_ATTEMPTS; i++) {
    try {
      debug('Capture attempt %s for %s', i, testDefinition.name)
      const res = await capture(testDefinition, matrix, captureFn, testFile, snapshotName)
      exception = null

      return res
    } catch (ex) {
      if (!ex.exportedPaths) {
        // not mismatch error, so throw exception straight away
        throw ex
      }

      exception = ex

      const captureDelay = CAPTURE_DELAY_BETWEEN_ATTEMPTS * 2 ** i // exponential back off for delay

      debug(
        'Failed capture attempt %s for %s, waiting %s ms',
        i,
        testDefinition.name,
        captureDelay
      )

      const isNotLastCapture = i < CAPTURE_ATTEMPTS - 1

      if (isNotLastCapture) {
        await new Promise(resolve => setTimeout(resolve, captureDelay))
      }
    }
  }

  if (exception != null) {
    throw exception
  }
}
