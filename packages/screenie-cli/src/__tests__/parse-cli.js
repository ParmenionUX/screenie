/* eslint-env mocha */

import { expect } from 'chai'
import { getFilteredMatrices } from '../parse-cli'

describe('parse-cli', function () {
  describe('getFilteredMatrices', function () {
    it('should return null when no options passed in', function () {
      expect(getFilteredMatrices(null)).to.be.null
    })

    it('should return filtered matrices from string input', function () {
      expect(
        getFilteredMatrices('browsers=chrome59,browsers=firefox,viewport=desktop')
      ).to.eql({
        browsers: ['chrome59', 'firefox'],
        viewport: ['desktop'],
      })
    })
  })
})
