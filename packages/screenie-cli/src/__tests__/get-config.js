/* eslint-env mocha */

import { expect } from 'chai'
import { getAllMatrices } from '../get-config'

describe('get-config', function () {
  describe('getAllMatrices', function () {
    it('should return all of the possible combinations', function () {
      const projectConfig = {
        matrices: {
          browser: ['chrome59', 'firefox50', 'ie8'],
          viewport: ['desktop', 'tablet', 'mobile'],
        },
      }

      const res = getAllMatrices(projectConfig, null)

      expect(res).to.eql([
        { browser: 'chrome59', viewport: 'desktop' },
        { browser: 'chrome59', viewport: 'tablet' },
        { browser: 'chrome59', viewport: 'mobile' },
        { browser: 'firefox50', viewport: 'desktop' },
        { browser: 'firefox50', viewport: 'tablet' },
        { browser: 'firefox50', viewport: 'mobile' },
        { browser: 'ie8', viewport: 'desktop' },
        { browser: 'ie8', viewport: 'tablet' },
        { browser: 'ie8', viewport: 'mobile' },
      ])
    })

    it('should return all of the possible combinations when filtering one of the matrices', function () {
      const projectConfig = {
        matrices: {
          browser: ['chrome59', 'firefox50', 'ie8'],
          viewport: ['desktop', 'tablet', 'mobile'],
        },
      }

      const filteredMatrices = {
        browser: ['chrome59'],
      }

      const res = getAllMatrices(projectConfig, filteredMatrices)

      expect(res).to.eql([
        { browser: 'chrome59', viewport: 'desktop' },
        { browser: 'chrome59', viewport: 'tablet' },
        { browser: 'chrome59', viewport: 'mobile' },
      ])
    })

    it('should return the only combinations when filtering all of the matrices', function () {
      const projectConfig = {
        matrices: {
          browser: ['chrome59', 'firefox50', 'ie8'],
          viewport: ['desktop', 'tablet', 'mobile'],
          theme: ['main', 'alternate'],
        },
      }

      const filteredMatrices = {
        browser: ['chrome59'],
        viewport: ['tablet'],
      }

      const res = getAllMatrices(projectConfig, filteredMatrices)

      expect(res).to.eql([
        { browser: 'chrome59', viewport: 'tablet', theme: 'main' },
        { browser: 'chrome59', viewport: 'tablet', theme: 'alternate' },
      ])
    })

    it("should ignore provided ignored matrices which aren't configured", function () {
      const projectConfig = {
        matrices: {
          browser: ['chrome59', 'firefox50', 'ie8'],
          viewport: ['desktop', 'tablet', 'mobile'],
        },
      }

      const filteredMatrices = {
        somethingWhichIsntConfigured: ['blah'],
      }

      const res = getAllMatrices(projectConfig, filteredMatrices)

      expect(res).to.eql([
        { browser: 'chrome59', viewport: 'desktop' },
        { browser: 'chrome59', viewport: 'tablet' },
        { browser: 'chrome59', viewport: 'mobile' },
        { browser: 'firefox50', viewport: 'desktop' },
        { browser: 'firefox50', viewport: 'tablet' },
        { browser: 'firefox50', viewport: 'mobile' },
        { browser: 'ie8', viewport: 'desktop' },
        { browser: 'ie8', viewport: 'tablet' },
        { browser: 'ie8', viewport: 'mobile' },
      ])
    })

    it('should limit the matrix depending on configuration', function () {
      const projectConfig = {
        matrices: {
          browser: ['chrome59', 'firefox50', 'ie8'],
          viewport: ['desktop', 'tablet', 'mobile'],
          theme: ['default', 'light'],
        },
        onlyRun: {
          browser: {
            firefox50: {
              viewport: ['desktop', 'mobile'],
            },
            ie8: {
              viewport: ['tablet'],
              theme: ['default'],
            },
          },
        },
      }

      const res = getAllMatrices(projectConfig, null)

      expect(res).to.eql([
        { browser: 'chrome59', viewport: 'desktop', theme: 'default' },
        { browser: 'chrome59', viewport: 'desktop', theme: 'light' },
        { browser: 'chrome59', viewport: 'tablet', theme: 'default' },
        { browser: 'chrome59', viewport: 'tablet', theme: 'light' },
        { browser: 'chrome59', viewport: 'mobile', theme: 'default' },
        { browser: 'chrome59', viewport: 'mobile', theme: 'light' },
        { browser: 'firefox50', viewport: 'desktop', theme: 'default' },
        { browser: 'firefox50', viewport: 'desktop', theme: 'light' },
        { browser: 'firefox50', viewport: 'mobile', theme: 'default' },
        { browser: 'firefox50', viewport: 'mobile', theme: 'light' },
        { browser: 'ie8', viewport: 'tablet', theme: 'default' },
      ])
    })
  })
})
