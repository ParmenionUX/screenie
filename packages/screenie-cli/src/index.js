import prog from 'commander'
import resolve from 'resolve'
import { getFilteredMatrices } from './parse-cli'
import hooks from './hooks'

process.on('unhandledRejection', err => {
  console.error('Got unhandled rejection')
  console.error(err.stack || err.message)
  console.error(err)
  process.exit(1)
})

const filesToRequire = []

const collectFilesToRequire = file => filesToRequire.push(file)

export const test = () => {
  throw new Error(
    'test(): Unexpected callsite, are you running this outside of the screenie CLI runner?'
  )
}

test.only = () => {
  throw new Error(
    'test(): Unexpected callsite, are you running this outside of the screenie CLI runner?'
  )
}

export const before = (promise) => hooks.before.push(promise)
export const after = (promise) => hooks.after.push(promise)

prog
  .version('1.0.0')
  .description('Run regression tests')
  .usage('<tests...>')
  .option(
    '-r, --require <file>',
    'File to require before running tests',
    collectFilesToRequire,
    []
  )
  .option(
    '-m, --matrix [name]',
    'Only run tests for the specified matrix names (comma separated, e.g --matrix browser=chrome59)'
  )
  .option(
    '-i, --interactive',
    "Don't run any tests by default and instead open a browser window where tests can manually be run"
  )
  .option('--json [file]', 'Output test results as JSON at the given path')
  .action(async (...args) => {
    const testGlobs = args.slice(0, args.length - 1)
    const opts = args[args.length - 1]

    // require files specified with -r
    for (const file of filesToRequire) {
      const mod = resolve.sync(file, { basedir: process.cwd() })
      require(mod)
    }

    const filteredMatrices = getFilteredMatrices(opts.matrix)

    await require('./cli').default(testGlobs, {
      filteredMatrices,
      outputJson: opts.json,
      interactiveMode: opts.interactive,
    })
  })

prog.parse(process.argv)
