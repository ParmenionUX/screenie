import _ from 'lodash'
import glob from 'glob-promise'
import reduce from 'promise-reduce'
import proxyquire from 'proxyquire'
import { runTestDefinition } from './run'
import * as status from './status'

const debug = require('debug')('screenie-cli:discover-tests')

export async function discoverTestFiles(testGlobs) {
  debug('Finding test files matching glob %s', testGlobs)

  if (Array.isArray(testGlobs)) {
    return await reduce(
      async (current, globPattern) => [
        ...current,
        ...(await glob(globPattern, { realpath: true })),
      ],
      []
    )(testGlobs)
  }

  return await glob(testGlobs, { realpath: true })
}

export function findTestsFromFile(filePath) {
  return { filePath, tests: getTestsInFile(filePath) }
}

export async function runTests(tests, testMatrix) {
  await Promise.all(
    testMatrix.map(async thisMatrix => {
      await Promise.all(
        tests.map(async test => {
          await runTest(test, thisMatrix)
        })
      )
    })
  )
}

/**
 * adds the tests to the runner but doesn't actually run them
 */
export async function addTests(tests, testMatrix) {
  await Promise.all(
    testMatrix.map(async matrix => {
      await Promise.all(
        tests.map(async test => {
          const matrixLabel = _.values(matrix).join('-')
          status.addTest(test, matrix, matrixLabel)
        })
      )
    })
  )
}

export async function runTest(test, matrix) {
  const matrixLabel = _.values(matrix).join('-')

  try {
    status.addTest(test, matrix, matrixLabel)
    status.logTestRun(test, matrixLabel)

    debug('Running test %s', test.name)

    const testRun = runTestDefinition(test, matrix, events => {
      events.on('worker.state', data =>
        debug('[%s] %s -> %s', test.name, data.from, data.to)
      )

      events.on('worker.snapshotPass', data => {
        status.logSnapshotSuccess(test, matrixLabel, data.snapshot, {
          isNew: data.isNew,
        })
      })

      events.on('worker.snapshotFail', data => {
        status.logSnapshotFail(test, matrixLabel, data.snapshot, {
          isMismatch: data.isMismatch,
          message: data.exception.message,
          exception: data.exception,
        })
      })
    })

    const res = await testRun

    if (res.isNew) {
      status.logTestSuccess(test, matrixLabel, { isNew: true })
    } else {
      status.logTestSuccess(test, matrixLabel)
    }
  } catch (ex) {
    if (ex.message.includes('Screenshot mismatch')) {
      status.logTestFail(test, matrixLabel, { isMismatch: true, exception: ex })
    } else {
      status.logTestFail(test, matrixLabel, {
        isMismatch: false,
        message: ex.message,
        exception: ex,
      })
    }
  }
}

export function removeNonExclusiveTestsIfSomeAreExclusive(testDefinitions) {
  let someTestsAreExclusive = false

  for (const test of testDefinitions) {
    if (test.isExclusive) {
      someTestsAreExclusive = true
      break
    }
  }

  if (!someTestsAreExclusive) return testDefinitions

  return testDefinitions.filter(test => test.isExclusive)
}

function getTestsInFile(filePath) {
  const testDefinitions = []

  const testFn = (name, definition) =>
    testDefinitions.push({ name, filePath, snapshots: definition.snapshots })

  testFn.only = (name, definition) =>
    testDefinitions.push({
      name,
      filePath,
      snapshots: definition.snapshots,
      isExclusive: true,
    })

  const stubbedModules = {
    'screenie-cli': {
      test: testFn,
    },
  }

  try {
    proxyquire(filePath, stubbedModules)
  } catch (ex) {
    console.error(ex.stack || ex.message)

    throw new Error('Error requiring test file ' + filePath)
  }

  debug('Got %s tests in file %s', testDefinitions.length, filePath)

  return testDefinitions
}
