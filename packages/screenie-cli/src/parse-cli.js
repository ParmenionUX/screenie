export function getFilteredMatrices(matrixOptions) {
  if (!matrixOptions) return null

  const splitOptions = matrixOptions.split(',').map(opt => opt.trim())

  const filteredMatrices = {}

  for (const splitOption of splitOptions) {
    const splitKeyAndValue = splitOption.split('=')
    const key = splitKeyAndValue[0]
    const val = splitKeyAndValue[1]

    filteredMatrices[key] = [...(filteredMatrices[key] || []), val]
  }

  return filteredMatrices
}
