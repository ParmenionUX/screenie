const path = require('path')

module.exports = {
  entry: './src/browser-reporter/index.js',
  output: {
    path: path.resolve(__dirname, 'resources/browser-reporter'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: 'babel-loader',
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(woff|woff2|png|eot|ttf|svg)/,
        use: 'file-loader',
      },
    ],
  },
}
