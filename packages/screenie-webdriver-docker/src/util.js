export function getDockerHost() {
  const dockerHost = process.env.DOCKER_HOST

  if (dockerHost && dockerHost.indexOf('tcp://') !== -1) {
    // Remote docker daemon (using something like docker-machine)

    let host = dockerHost.substring('tcp://'.length)
    host = host.substring(0, host.lastIndexOf(':')).trim()

    return host
  }

  // No custom docker host, assume localhost
  return 'localhost'
}
