import {execSync} from 'child_process'
import invariant from 'invariant'
import {getDockerHost} from './util'

// we keep these out of the scope of getVNCConnectionDetails() so they are cached in between calls
let dockerNodeIds = null
let dockerInspect = null

/*
 * This function takes a WebdriverIO browser instance, finds out the private IP address of the node
 * and then looks up the information for that container in Docker. We then look at the network config
 * for the container and find out which port on localhost the VNC server is forwarded to (VNC server
 * listens on port 5900 in the container, see docker-compose.yml). Once we have the port, we return
 * that as well was the host which will be localhost when Docker Daemon is running locally, or something
 * else when using Docker Toolbox with a VM or docker-machine.
 */
export async function generateVNCConnectionDetails(browser) {
  invariant(
    browser != null,
    'getVNCConnectionDetails(): browser is null, are you using screenie-webdriver in your tests?',
  )

  const nodeDetails = await browser.getGridNodeDetails()
  if (!nodeDetails || !nodeDetails.request) return null

  const nodeIP = nodeDetails.request.configuration.host.trim()

  if (!dockerNodeIds) {
    dockerNodeIds = execSync('docker-compose ps -q', {
      cwd: process.cwd(),
    }).toString().trim().split('\n')
  }

  if (!dockerInspect) {
    dockerInspect = JSON.parse(execSync(`docker inspect ${dockerNodeIds.join(' ')}`).toString())
  }

  // eslint-disable-next-line
  const dockerContainer = dockerInspect.find(container => {
    const containerNetworks = Object.keys(container.NetworkSettings.Networks)
    const containerDefaultNetwork = container.NetworkSettings.Networks[containerNetworks[0]]
    if (!containerDefaultNetwork) return false

    return containerDefaultNetwork.IPAddress === nodeIP
  })

  const vncPort = dockerContainer.NetworkSettings.Ports['5900/tcp'][0].HostPort

  return {
    host: getDockerHost(),
    port: vncPort,
    password: 'secret', // this is the password which is baked into the selenium/node-chrome-debug images, see https://github.com/SeleniumHQ/docker-selenium#debugging
    width: 1360, // this is the screen resolution of the selenium/node-chrome-debug images
    height: 1020,
  }
}
