import {getDockerHost} from './util'

export const webdriverConnectionDetails = {
  host: getDockerHost(),
  port: process.env.SCREENIE_DOCKER_WEBDRIVER_HUB_PORT, // this is generated in ../env.js
}
