import * as webdriverio from 'webdriverio'
import { init as initWdioScreenshot } from 'wdio-screenshot'
import invariant from 'invariant'

const debug = require('debug')('screenie-webdriver:browser-pool')

const createPoolFactory = webdriverOpts => ({
  async create() {
    debug('Creating new browser instance with opts', webdriverOpts)

    const browser = webdriverio.remote({
      ...webdriverOpts,
      baseUrl: undefined,
    })

    initWdioScreenshot(browser)

    debug('Initialising browser')
    await browser.init()

    debug('Setting viewport size')
    browser.__viewportSize = webdriverOpts.viewportSize
    try {
      await browser.setViewportSize({ width: 1920, height: 1080 })
      await browser.url('about:blank')

      await browser.setViewportSize(
        webdriverOpts.viewportSize || { width: 1280, height: 720 }
      )
    } catch (ex) {
      debug('FAILED to set viewport size (this can happen on some browsers, especially when using remote services like Browserstack)')
    }

    return { browser }
  },

  async destroy({ browser }) {
    debug('Ending browser session')
    await browser.end()
  },
})

const getWebdriverOptsUniqueKey = webdriverOpts => {
  // stringify webdriveropts to get a unique key.
  // if the webdriveropts key is the same between two sessions, it can recycle the first browser
  // session without ending and starting a new selenium session.
  // We ignore baseurl and viewportSize from the key as they can be changed during a selenium session.

  // TODO stable stringify?
  const stringified = JSON.stringify({
    ...webdriverOpts,
    baseUrl: undefined,
    viewportSize: undefined,
  })

  return stringified
}

const browserPools = {}

function getBrowserPool(browserPoolName = 'default') {
  if (!browserPools[browserPoolName]) {
    browserPools[browserPoolName] = { browsers: [], queuedRequests: [], startBrowsersTimeout: null }
  }

  return browserPools[browserPoolName]
}

function getNextRequestForBrowser(browserPoolName) {
  const { browsers, queuedRequests } = getBrowserPool(browserPoolName)

  // try and find a request with options which doesn't have a browser yet
  for (const req of queuedRequests) {
    if (
      !browsers.find(
        _browser => _browser.stringifiedWebdriverOpts === req.stringifiedWebdriverOpts
      )
    ) {
      return req
    }
  }

  // TODO find request with least amount of browsers
  // for now just return first one

  return queuedRequests[0]
}

export async function getBrowserInstance(webdriverOpts, browserPoolConcurrency = {}) {
  const browserPoolName = webdriverOpts.browserPool
  const pool = getBrowserPool(browserPoolName)
  const concurrency = browserPoolConcurrency[browserPoolName] || 1
  const { browsers, queuedRequests } = pool

  const stringifiedWebdriverOpts = getWebdriverOptsUniqueKey(webdriverOpts)

  if (!pool.startBrowsersTimeout) {
    pool.startBrowsersTimeout = setTimeout(() => {
      for (let i = browsers.length; i < concurrency; i++) {
        const nextRequest = getNextRequestForBrowser(browserPoolName)
        if (nextRequest == null) break
        queuedRequests.splice(queuedRequests.indexOf(nextRequest), 1)
        debug('Starting browser %s/%s in pool %s with opts', i, concurrency, browserPoolName, nextRequest.stringifiedWebdriverOpts)

        const newBrowser = {
          browserInstance: null,
          webdriverOpts: nextRequest.webdriverOpts,
          stringifiedWebdriverOpts: getWebdriverOptsUniqueKey(nextRequest.webdriverOpts),
        }

        browsers.push(newBrowser)

        debug('Amount of browsers in pool %s after add is now %s', browserPoolName, browsers.length)
        ;(async function () {
          const browserInstanceWrapper = await createPoolFactory(
            nextRequest.webdriverOpts
          ).create()

          newBrowser.browserInstance = browserInstanceWrapper.browser
          nextRequest.callback(newBrowser)
        })()
      }

      pool.startBrowsersTimeout = null
    }, 500)
  }

  // queue up request for browser slot
  const request = {
    webdriverOpts,
    stringifiedWebdriverOpts,
    callback: null,
  }

  const waitPromise = new Promise(resolve => (request.callback = resolve))

  queuedRequests.push(request)

  debug('Waiting for browser from pool', browserPoolName)

  const browser = await waitPromise
  debug('Acquired browser from pool', browser)

  debug('Viewport size', JSON.stringify(browser.browserInstance.__viewportSize), JSON.stringify(webdriverOpts.viewportSize))

  if (JSON.stringify(browser.browserInstance.__viewportSize) !== JSON.stringify(webdriverOpts.viewportSize)) {
    debug('Updating viewport size for existing session')
    browser.browserInstance.__viewportSize = webdriverOpts.viewportSize
    try {
      await browser.browserInstance.setViewportSize(
        webdriverOpts.viewportSize || { width: 1280, height: 720 }
      )
    } catch (ex) {
      debug('FAILED to set viewport size (this can happen on some browsers, especially when using remote services like Browserstack)', ex)
    }
  }

  return { browser: browser.browserInstance }
}

export async function freeBrowser(browserWrapper, webdriverOpts, browserPoolConcurrency = {}) {
  const browserPoolName = webdriverOpts.browserPool
  const { browsers, queuedRequests } = getBrowserPool(browserPoolName)
  const concurrency = browserPoolConcurrency[browserPoolName] || 1

  const stringifiedWebdriverOpts = getWebdriverOptsUniqueKey(webdriverOpts)
  const browser = browsers.find(
    _browser => _browser.browserInstance === browserWrapper.browser
  )

  invariant(
    !!browser,
    'freeBrowser(): Browser not found in pool from given browser instance'
  )

  // see if there are any requests with the same webdriver opts
  const requestsWithSameWebdriverOpts = queuedRequests.filter(
    request => request.stringifiedWebdriverOpts === stringifiedWebdriverOpts
  )

  if (requestsWithSameWebdriverOpts.length > 0) {
    const nextRequest = requestsWithSameWebdriverOpts[0]
    queuedRequests.splice(queuedRequests.indexOf(nextRequest), 1)

    debug('Recycling browser session to request with same webdriver options', nextRequest)

    browser.webdriverOpts = webdriverOpts

    nextRequest.callback(browser)
    return
  }

  // dispose of this browser
  await createPoolFactory(webdriverOpts).destroy(browserWrapper)
  browsers.splice(browsers.indexOf(browser), 1)
  debug('Amount of browsers in pool after remove is now %s/%s', browsers.length, concurrency)

  // create a new browser if one is needed
  if (queuedRequests.length > 0 && browsers.length < concurrency) {
    const nextRequest = queuedRequests.shift()

    const newBrowser = {
      browserInstance: null,
      webdriverOpts: nextRequest.webdriverOpts,
      stringifiedWebdriverOpts: getWebdriverOptsUniqueKey(nextRequest.webdriverOpts),
    }

    browsers.push(newBrowser)
    debug('Amount of browsers in pool after add is now %s/%s', browsers.length, concurrency)

    const browserInstanceWrapper = await createPoolFactory(
      nextRequest.webdriverOpts
    ).create()

    newBrowser.browserInstance = browserInstanceWrapper.browser
    nextRequest.callback(newBrowser)
  }
}
