import { getTemporaryScreenshotPath } from 'screenie-core'
import { getBrowserInstance, freeBrowser } from './browser-pool'

const debug = require('debug')('screenie-webdriver:webdriver')

export const createWebdriver = opts => {
  const webdriverConfigFn = opts.config
  const { browserPoolConcurrency } = opts

  return {
    testRoute(pathname) {
      let testOpts, definition

      // arguments can either be
      // pathname, testOpts, definition
      // pathname, definition

      if (typeof arguments[1] === 'object') {
        testOpts = arguments[1] || {}
        definition = arguments[2]
      } else if (typeof arguments[1] === 'function') {
        testOpts = {}
        definition = arguments[1]
      } else {
        testOpts = {}
      }

      if (!definition) {
        definition = function () {}
      }

      let browser = null
      let browserWrapper = null
      let ctx = null

      const { snapshots, captures, beforeHooks } = compileDefinition(definition)

      return {
        snapshots,

        run: matrix => {
          const webdriverOpts = webdriverConfigFn(matrix)

          return {
            snapshots,

            async onenteredpending() {
              debug('Getting browser from pool')

              browserWrapper = await getBrowserInstance(webdriverOpts, browserPoolConcurrency)
              browser = browserWrapper.browser

              debug('Navigating to %s', pathname)
              await browser.url(`${webdriverOpts.baseUrl}${pathname}`)
            },

            async onenteredpreparing() {
              ctx = {}

              for (const hook of beforeHooks) {
                await hook.call(ctx, browser)
              }
            },

            async onenteredpreparingCapture(fnOpts) {
              const { args: [snapshotName] } = fnOpts

              if (testOpts.delay) {
                await browser.pause(testOpts.delay)
              }

              const captureDefinition = captures.find(capture => capture.name === snapshotName)

              if (captureDefinition != null && captureDefinition.prepareCallback != null) {
                await captureDefinition.prepareCallback.call(ctx, browser)
              }
            },

            async onenteredcapturing(fnOpts) {
              const { args: [snapshotName] } = fnOpts

              const tmpScreenshotPath = getTemporaryScreenshotPath()

              const captureDefinition = captures.find(capture => capture.name === snapshotName)

              if (captureDefinition != null && captureDefinition.opts.fullPage) {
                if (
                  webdriverOpts.desiredCapabilities == null ||
                  webdriverOpts.desiredCapabilities.browserName !== 'internet explorer'
                ) {
                  await browser.saveDocumentScreenshot(tmpScreenshotPath)
                } else {
                  // internet explorer takes full page screenshots anyway so don't waste time with `saveDocumentScreenshot`
                  await browser.saveScreenshot(tmpScreenshotPath)
                }
              } else if (captureDefinition != null && captureDefinition.opts.element) {
                await browser.saveElementScreenshot(
                  tmpScreenshotPath,
                  captureDefinition.opts.element
                )
              } else {
                await browser.saveScreenshot(tmpScreenshotPath)
              }

              return (fnOpts.res = tmpScreenshotPath)
            },

            async onenteredexiting() {
              debug('Freeing browser session')
              await freeBrowser(browserWrapper, webdriverOpts)
            },
          }
        },
      }
    },
  }
}

function compileDefinition(definition) {
  const beforeHooks = []
  const captures = []

  const before = callback => beforeHooks.push(callback)

  function capture(name) {
    let opts = {}
    let prepareCallback

    if (typeof arguments[1] === 'object') {
      opts = arguments[1] || {}
      prepareCallback = arguments[2]
    } else if (typeof arguments[1] === 'function') {
      opts = {}
      prepareCallback = arguments[1]
    } else {
      opts = {}
    }

    captures.push({ name, opts, prepareCallback })
  }

  definition({ before, capture })

  const snapshots = captures.map(capture => capture.name)

  return { snapshots, captures, beforeHooks }
}
