export class ScreenshotMismatchError extends Error {
  constructor({ title, newScreenshotPath, referenceScreenshotPath, diffOutputPath, exportedPaths }) {
    super()

    this.message = 'Screenshot mismatch for test ' + title

    this.title = title
    this.newScreenshotPath = newScreenshotPath
    this.referenceScreenshotPath = referenceScreenshotPath
    this.diffOutputPath = diffOutputPath
    this.exportedPaths = exportedPaths
  }
}
