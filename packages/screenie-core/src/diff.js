import path from 'path'
import fs from 'fs-promise'
import mkdirp from 'mkdirp-promise'

export async function exportDiff(diff, referenceScreenshotPath, newScreenshotPath, dir) {
  await mkdirp(dir)

  const exportedReferencePath = path.join(dir, 'reference.png')
  const exportedNewPath = path.join(dir, 'new.png')
  const exportedDiffPath = path.join(dir, 'diff.png')
  const exportedHtmlPath = path.join(dir, 'index.html')

  await fs.copy(referenceScreenshotPath, exportedReferencePath)
  await fs.copy(newScreenshotPath, exportedNewPath)
  await writeDiffToFile(diff, exportedDiffPath)

  await writeHtmlDiff(exportedHtmlPath, diff.width, diff.height)

  return {
    reference: exportedReferencePath,
    new: exportedNewPath,
    diff: exportedDiffPath,
    html: exportedHtmlPath,
  }
}

async function writeDiffToFile(diff, diffPath) {
  const packDiff = diff.pack()
  packDiff.pipe(fs.createWriteStream(diffPath))
  await new Promise(resolve => packDiff.once('end', resolve))
}

async function writeHtmlDiff(diffHtmlOutputPath, maxWidth, maxHeight) {
  await fs.writeFile(diffHtmlOutputPath, `
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Screenshot diff</title>
  <style type="text/css">
    body {
      font-family: sans-serif;
    }

    #overlay-slider {
      display: block;
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      width: 100%;
      -webkit-appearance: none;
      background: transparent;
      overflow: hidden;
      outline: none !important;
      cursor: ew-resize;
    }

    #overlay-slider::-webkit-slider-thumb {
      -webkit-appearance: none;
      background: #000;
      border-radius: 0;
      width: 5px;
      height: 10000px;
    }

    .image-slider {
      position: relative;
      display: inline-block;
      line-height: 0;
      width: ${maxWidth}px;
      height: ${maxHeight}px;
    }

    .image-slider > #overlay-image {
      position: absolute;
      top: 0; bottom: 0; left: 0;
      max-width: 100%;
      overflow: hidden;
      background: white;
    }

    .image-slider img {
      user-select: none;
      max-width: 95vw;
    }
  </style>
</head>
<body>
  <p>
    <details>
      <summary>Diff</summary>

      <img src="diff.png">
    </details>
  </p>

  <p>
    <details>
      <summary>New / Original</summary>

      <div class="image-slider">
        <div id="overlay-image"><img src="new.png"></div>
        <img src="reference.png">

        <input type="range" min="0" max="100" value="50" step="0.1" oninput="handleSliderChange()" id="overlay-slider">
      </div>
    </details>
  </p>

  <script>
    var overlayImage = document.getElementById("overlay-image");
    var slider = document.getElementById("overlay-slider");

    function handleSliderChange() {
      overlayImage.style.width = slider.value+"%";
    }
  </script>
</body>
</html>
  `.trim())
}
