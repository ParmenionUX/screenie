import path from 'path'
import os from 'os'
import invariant from 'invariant'
import exists from 'path-exists'
import mkdirp from 'mkdirp-promise'
import fs from 'fs-promise'
import { PNG } from 'pngjs'
import pixelmatch from 'pixelmatch'
import shortid from 'shortid'
import { ScreenshotMismatchError } from './errors'
import { exportDiff } from './diff'

export * from './errors'

const tmpFolder = path.join(os.tmpdir(), 'screenietmp')
mkdirp(tmpFolder)

function readPNG(pngPath) {
  return new Promise(resolve => {
    const stream = fs
      .createReadStream(pngPath)
      .pipe(new PNG())
      .on('parsed', () => resolve(stream))
  })
}

export async function matchScreenshot(
  title,
  referenceScreenshotPath,
  newScreenshotPath,
  diffOutputPath
) {
  invariant(!!title, 'matchScreenshot(): No title supplied')
  invariant(!!referenceScreenshotPath, 'matchScreenshot(): No referenceScreenshotPath supplied')
  invariant(!!newScreenshotPath, 'matchScreenshot(): No newScreenshotPath supplied')
  invariant(!!diffOutputPath, 'matchScreenshot(): No diffOutputPath supplied')

  if (!await exists(referenceScreenshotPath)) {
    // screenshot doesn't exist so copy new one
    await mkdirp(path.dirname(newScreenshotPath))
    await fs.copy(newScreenshotPath, referenceScreenshotPath)

    return {
      isNew: true,
      success: true,
    }
  }

  const res = await isDifferent(referenceScreenshotPath, newScreenshotPath)

  if (res !== false) {
    const { diff } = res

    const exportedPaths = await exportDiff(
      diff,
      referenceScreenshotPath,
      newScreenshotPath,
      diffOutputPath
    )

    throw new ScreenshotMismatchError({
      title,
      referenceScreenshotPath,
      newScreenshotPath,
      diffOutputPath,
      exportedPaths,
    })
  } else {
    return {
      isNew: false,
      success: true,
    }
  }
}

export async function isDifferent(referenceScreenshotPath, newScreenshotPath) {
  invariant(!!referenceScreenshotPath, 'isDifferent(): No referenceScreenshotPath supplied')
  invariant(!!newScreenshotPath, 'isDifferent(): No newScreenshotPath supplied')

  const [existingImage, newImage] = await Promise.all([
    readPNG(referenceScreenshotPath),
    readPNG(newScreenshotPath),
  ])

  const maxWidth = Math.max(existingImage.width, newImage.width)
  const maxHeight = Math.max(existingImage.height, newImage.height)

  const diff = new PNG({ width: maxWidth, height: maxHeight })

  const changedPixels = pixelmatch(
    existingImage.data,
    newImage.data,
    diff.data,
    maxWidth,
    maxHeight,
    { threshold: 0.07 }
  )

  if (
    changedPixels > 0 ||
    existingImage.width !== newImage.width ||
    existingImage.height !== newImage.height
  ) {
    return { diff }
  }

  return false
}

export const updateScreenshot = (newScreenshotPath, referenceScreenshotPath) =>
  fs.copy(newScreenshotPath, referenceScreenshotPath)

export const getTemporaryScreenshotPath = () =>
  path.join(tmpFolder, `${shortid.generate()}-new.png`)
