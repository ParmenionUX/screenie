const ip = require('ip')

const { test } = require('screenie-cli')
const { webdriverConnectionDetails } = require('../../../packages/screenie-webdriver-docker')
const { createWebdriver } = require('../../../packages/screenie-webdriver')

const { testRoute } = createWebdriver({
  config: matrix =>
    Object.assign({}, webdriverConnectionDetails, {
      desiredCapabilities: {
        browserName: 'chrome',
      },
      baseUrl: `http://${ip.address()}:8035`,
    }),
})

//

test('hello world', testRoute('/'))
