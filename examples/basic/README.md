Screenie example
================

To run this example, `cd` to this directory and run:

```sh
$ npm install
$ npm test
```

Have a look at `run-tests.sh` which is a helper bash script which contains the boilerplate for spinning up the Selenium stack in Docker and running the static site using the `http-server` package.
