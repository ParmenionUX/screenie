#!/usr/bin/env bash
set -e

eval "$(../../packages/screenie-webdriver-docker/env)"

# Set concurrency for screenie webdriver
export SCREENIE_WEBDRIVER_BROWSERS=5

# Start docker environment
docker-compose up -d --scale chrome=$SCREENIE_WEBDRIVER_BROWSERS

# Run tests
./node_modules/.bin/concurrently --kill-others --success first --raw \
  "node ../../packages/screenie-cli/bin/screenie.js tests/**.js" "http-server -p 8035 static"
