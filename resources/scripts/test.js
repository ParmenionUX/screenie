/* eslint-disable */

require('shelljs/global')
set('-e')

console.log('Linting...')
exec('npm run lint')

console.log('Unit tests...')
exec('npm run unit')

console.log('Integration tests...')
exec('npm run integration')
