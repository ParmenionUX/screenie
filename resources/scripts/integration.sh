#!/usr/bin/env sh

set -e

# Set up environment variables
eval "$(./packages/screenie-webdriver-docker/env)"

# Start docker environment
docker-compose up -d
docker-compose scale chrome=4

# Run tests
npm run cucumber "$@"
