#!/usr/bin/env sh

set -e

# Set up environment variables
eval "$(./packages/screenie-webdriver-docker/env)"

# Run tests
cucumberjs -r test/integration/support/preload.js -r test/integration/support/hooks.js -r test/integration/support -r test/integration/features/__steps__ --fail-fast test/integration/features "$@"
