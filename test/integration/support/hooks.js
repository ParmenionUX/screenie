import { defineSupportCode } from 'cucumber'
import rimraf from 'rimraf'
// import path from 'path'
import liveServer from 'live-server'

defineSupportCode(({ Before, After, setDefaultTimeout }) => {

  setDefaultTimeout(120 * 1000)

  Before(function () {
    // rimraf.sync(path.join(__dirname, '../workspaces/*/tests/__screenshots__'))
    // rimraf.sync(path.join(__dirname, '../workspaces/*/diff_output'))
    rimraf.sync(this.jsonReportPath)
  })

  After(async function () {
    if (this.liveserver) {
      liveServer.shutdown()
    }

    if (this.browser) {
      await this.browser.end()
    }

    if (this.runner && !this.runner.exited) {
      this.runner.kill()
    }
  })

})
