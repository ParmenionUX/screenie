import path from 'path'
import fs from 'fs'
import liveServer from 'live-server'
import { defineSupportCode } from 'cucumber'

function World() {
  this.workspace = path.join(__dirname, '../workspaces/simple')

  this.jsonReportPath = path.join(__dirname, '../test-json-report.tmp.json')

  this.readJsonReport = () =>
    JSON.parse(fs.readFileSync(this.jsonReportPath))

  this.startServer = () => {
    this.liveserver = liveServer.start({
      port: 3035,
      root: this.workspace,
      open: false,
      logLevel: 0,
      watch: ['**/*.html'],
      ignorePattern: /diff_output/,
    })
  }
}

defineSupportCode(({ setWorldConstructor }) =>
  setWorldConstructor(World)
)
