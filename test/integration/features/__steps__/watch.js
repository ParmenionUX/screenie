import { defineSupportCode } from 'cucumber'
import path from 'path'
import cp from 'cp-file'
import Jimp from 'jimp'
import { expect } from 'chai'
import * as webdriverio from 'webdriverio'
import ip from 'ip'
import Automator from '../../util/cli-automator'
import { webdriverConnectionDetails } from '../../../../packages/screenie-webdriver-docker'
import { matchScreenshot, getTemporaryScreenshotPath } from '../../../../packages/screenie-core'

defineSupportCode(({ Given, When, Then }) => {

  Given('I have screenie running with webdriver in watch mode with a few tests', async function () {
    this.workspace = path.join(__dirname, '../../workspaces/watch')

    await cp(
      path.join(this.workspace, 'before.html'),
      path.join(this.workspace, 'index.html'),
    )

    this.startServer()

    this.runner = await Automator.spawn(['-r', 'babel-register', '-r', 'babel-polyfill', '--watch', 'tests/**/*.js'], {
      cwd: this.workspace,
    })

    await this.runner.waitForMatch(/waiting for changes/)
  })

  Given('I have the interactive watch mode browser reporter open', async function () {
    const browserReporterPort = this.runner.getOutput().match(/View tests running live at http:\/\/localhost:(\d+)/)[1]

    this.browser = webdriverio.remote({
      ...webdriverConnectionDetails,
      desiredCapabilities: {
        browserName: 'chrome',
      },
      baseUrl: `http://${ip.address()}:${browserReporterPort}`,
    })

    await this.browser.init()
    await this.browser.setViewportSize({ width: 1280, height: 720 })
    await this.browser.url('/')
  })

  When('I make non breaking changes to the code for the underlying site', async function () {
    await cp(
      path.join(this.workspace, 'nonbreaking.html'),
      path.join(this.workspace, 'index.html'),
    )
  })

  When('I make breaking changes to the code for the underlying site', async function () {
    await cp(
      path.join(this.workspace, 'breaking.html'),
      path.join(this.workspace, 'index.html'),
    )
  })

  When('the underlying site reloads', async function () {
    // wait a bit so webpage can reload and tests can re-run
    await this.runner.waitForMatch(/RUNNING/)
  })

  When('I view some test details in the browser reporter', async function () {
    await this.browser.click('[data-testid=testlistitem-start]')
    await this.browser.waitForVisible('h1=start')
  })

  When('I enable a live preview of the test', async function () {
    await this.browser.click('button=Show live preview')
  })

  Then('the tests should run', async function () {
    expect(this.runner.getOutput()).to.contain('RUNNING')
  })

  Then('the tests should pass during watching', async function () {
    await this.runner.waitForMatch(/SUCCESS/)

    expect(this.runner.getOutput()).to.contain('SUCCESS')
    expect(this.runner.getOutput()).to.not.contain('FAILURE')
  })

  Then('the tests should fail during watching', async function () {
    await this.runner.waitForMatch(/FAILURE/)

    expect(this.runner.getOutput()).to.contain('FAILURE')
    expect(this.runner.getOutput()).to.not.contain('SUCCESS')
  })

  Then('I should see a live preview of the test', async function () {
    // wait for live preview to appear
    // TODO can we not use a hard coded delay here?
    await new Promise(resolve => setTimeout(resolve, 15000))

    // blank out noVNC status bar as it has the IP address of the container which varies
    // between test run
    await this.browser.execute(function () {
      // eslint-disable-next-line
      var element = document.getElementsByTagName('iframe')[0].contentDocument.getElementById('noVNC_status')
      element.style.opacity = 0
    })

    const screenshotPath = getTemporaryScreenshotPath()

    // take screenshot
    await this.browser.saveScreenshot(screenshotPath)

    // crop out some of the preview windows as there will be stuff which we don't care about (IP addresses, ports)
    // and will cause the diff to fail
    const screenshotJimp = await Jimp.read(screenshotPath)
    await new Promise(resolve => screenshotJimp.crop(0, 300, 1280, 720 - 300).write(screenshotPath, () => resolve()))

    // compare screenshot of browser reporter using screenie itself!
    await matchScreenshot(
      'browser reporter live preview',
      path.join(__dirname, '__screenshots__/browser-reporter-live-preview.png'),
      screenshotPath,
      path.join(__dirname, 'diff_output'),
    )
  })

})
