import { defineSupportCode } from 'cucumber'
import fs from 'mz/fs'
import path from 'path'
import cpy from 'cpy'
import chai, { expect } from 'chai'
import chaiSubset from 'chai-subset'
import Automator from '../../util/cli-automator'

chai.use(chaiSubset)

defineSupportCode(({ Given, When, Then }) => {

  Given('I have reference screenshots', async function () {
    await cpy(
      path.join(__dirname, '../../workspaces/reference-screenshots/*.png'),
      path.join(this.workspace, 'tests/__screenshots__/'),
    )
  })

  Given('I\'ve made breaking changes', async function () {
    this.workspace = path.join(__dirname, '../../workspaces/breaking')
  })

  When('I run screenie with webdriver with a few tests', async function () {
    this.startServer()

    this.runner = await Automator.spawn(['-r', 'babel-register', '-r', 'babel-polyfill', '--json', this.jsonReportPath, 'tests/**/*.js'], {
      cwd: this.workspace,
    })

    await this.runner.waitForLaunch()
  })

  When('I don\'t take any action on screenshot mismatches', async function () {
    // Choose action on interactive prompt for screenshot mismatch
    await this.runner.waitForMatch(/Screenshot mismatch found for test/)

    await this.runner.send('down') // Ignore and continue
    await this.runner.send('enter') // ENTER

    await this.runner.send('down') // Ignore and continue
    await this.runner.send('enter') // ENTER
  })

  When('I update the reference screenshots with the new screenshots', async function () {
    // Choose action on interactive prompt for screenshot mismatch
    await this.runner.waitForMatch(/Screenshot mismatch found for test/)

    await this.runner.send('down') // Ignore and continue
    await this.runner.send('down') // Update screenshot
    await this.runner.send('enter') // ENTER

    await this.runner.send('down') // Ignore and continue
    await this.runner.send('down') // Update screenshot
    await this.runner.send('enter') // ENTER
  })

  Then('I should see reference screenshots which have been saved', async function () {
    await this.runner.waitForDone()

    expect(
      await fs.exists(path.join(this.workspace, 'tests/__screenshots__/sample-start.png'))
    ).to.be.true
  })

  Then('the tests should pass', async function () {
    await this.runner.waitForDone()

    const jsonReport = this.readJsonReport()
    expect(jsonReport).to.containSubset({
      files: [{
        tests: [
          {
            name: 'start',
            passed: true,
            snapshots: [
              { name: null, passed: true },
            ],
          },
          {
            name: 'details dropdown',
            passed: true,
            snapshots: [
              { name: 'initial', passed: true },
              { name: 'open', passed: true },
              { name: 'closed', passed: true },
            ],
          },
        ],
      }],
    })
  })

  Then('the tests should fail', async function () {
    await this.runner.waitForDone()

    expect(this.runner.output).to.contain('Screenshot mismatch for test start')
    expect(this.runner.output).to.contain('Screenshot mismatch for test details dropdown')

    const jsonReport = this.readJsonReport()

    expect(jsonReport.files[0].tests).to.containSubset([
      {
        name: 'start',
        passed: false,
        snapshots: [
          { name: null, passed: false },
        ],
      },
      {
        name: 'details dropdown',
        passed: false,
        snapshots: [
          { name: 'initial', passed: true },
          { name: 'open', passed: false },
          { name: 'closed', passed: true },
        ],
      },
    ])
  })

})
