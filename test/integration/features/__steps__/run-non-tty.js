import { defineSupportCode } from 'cucumber'
import Automator from '../../util/cli-automator'

defineSupportCode(({ Given, When, Then }) => {

  When('I run screenie in a non TTY with webdriver with a few tests', async function () {
    this.startServer()

    this.runner = await Automator.spawn(['-r', 'babel-register', '-r', 'babel-polyfill', '--json', this.jsonReportPath, 'tests/**/*.js'], {
      cwd: this.workspace,
      tty: false,
    })

    await this.runner.waitForLaunch()
  })

})
