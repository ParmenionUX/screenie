Feature: Watch mode

  Scenario: Watching tests
    Given I have screenie running with webdriver in watch mode with a few tests
    And I have reference screenshots
    When I make non breaking changes to the code for the underlying site
    And the underlying site reloads
    Then the tests should run
    And the tests should pass during watching

  Scenario: Watching tests with breaking changes
    Given I have screenie running with webdriver in watch mode with a few tests
    And I have reference screenshots
    When I make breaking changes to the code for the underlying site
    And the underlying site reloads
    Then the tests should run
    And the tests should fail during watching

#  Scenario: View live preview of watching tests in browser reporter
#    Given I have screenie running with webdriver in watch mode with a few tests
#    And I have reference screenshots
#    And I have the interactive watch mode browser reporter open
#    When I view some test details in the browser reporter
#    And I enable a live preview of the test
#    Then I should see a live preview of the test
