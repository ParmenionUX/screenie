Feature: Run tests with a non interactive terminal

  Scenario: Run tests without reference screenshots with non interactive terminal
    When I run screenie in a non TTY with webdriver with a few tests
    Then I should see reference screenshots which have been saved
    And the tests should pass

  Scenario: Run tests with reference screenshots with non interactive terminal
    Given I have reference screenshots
    When I run screenie in a non TTY with webdriver with a few tests
    Then the tests should pass

  Scenario: Run tests with breaking changes against reference screenshots with non interactive terminal
    Given I've made breaking changes
    And I have reference screenshots
    And I've made breaking changes
    When I run screenie in a non TTY with webdriver with a few tests
    Then the tests should fail
