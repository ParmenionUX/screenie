Feature: Run tests

  Scenario: Run tests without reference screenshots
    When I run screenie with webdriver with a few tests
    Then I should see reference screenshots which have been saved
    And the tests should pass

  Scenario: Run tests with reference screenshots
    Given I have reference screenshots
    When I run screenie with webdriver with a few tests
    Then the tests should pass

  Scenario: Run tests with breaking changes against reference screenshots
    Given I've made breaking changes
    And I have reference screenshots
    And I've made breaking changes
    When I run screenie with webdriver with a few tests
    And I don't take any action on screenshot mismatches
    Then the tests should fail

  Scenario: Run tests with breaking changes against reference screenshots and update reference screenshots
    Given I've made breaking changes
    And I have reference screenshots
    And I've made breaking changes
    When I run screenie with webdriver with a few tests
    And I update the reference screenshots with the new screenshots
    And I don't take any action on screenshot mismatches
    Then the tests should pass
