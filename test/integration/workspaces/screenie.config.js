const ip = require('ip')
const {
  generateVNCConnectionDetails,
  webdriverConnectionDetails,
} = require('../../../packages/screenie-webdriver-docker')

export const opts = {
  webdriver: {
    ...webdriverConnectionDetails,
    desiredCapabilities: {
      browserName: 'chrome',
    },
    baseUrl: `http://${ip.address()}:3035`,
  },
  getVNCConnectionDetails: async browser => ({
    ...(await generateVNCConnectionDetails(browser)),
    host: ip.address(),
  }),
}
