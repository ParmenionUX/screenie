import { test } from '../../../../../packages/screenie-cli'
import { testRoute, click, capture, pause } from '../../../../../packages/screenie-webdriver'
import { opts } from '../../screenie.config'

test('start', testRoute('/index.html', opts))

test('details dropdown', testRoute('/details.html', opts, function * () {
  yield capture('initial')

  yield click('details summary')

  yield pause(500)

  yield capture('open')

  yield click('details summary')

  yield capture('closed')
}))
