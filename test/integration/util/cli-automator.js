import path from 'path'
import pty from 'ptyw.js'
import stripAnsi from 'strip-ansi'
import {spawn} from 'child_process'

// from http://stackoverflow.com/questions/17470554/how-to-capture-the-arrow-keys-in-node-js
const keyCodes = {
  'up': '\u001b[A',
  'down': '\u001b[B',
  'right': '\u001b[C',
  'left': '\u001b[D',
  'enter': '\n',
  'space': ' ',
}

const SYNTHETIC_DELAY_MS = 150

export const SCREENIE_BIN = path.join(__dirname, '../../../packages/screenie-cli/bin/screenie.js')

export default class Automator {

  static spawn(args, opts = {}) {
    const automator = new this(args, opts)
    automator.spawn()
    return automator
  }

  constructor(args, opts) {
    this.args = args
    this.opts = opts
  }

  spawn() {
    console.log()
    console.log(' >', SCREENIE_BIN, this.args.join(' '))
    console.log()

    this.output = ''

    if (this.opts.tty === false) {
      this.term = spawn('node', [SCREENIE_BIN, ...this.args], {
        env: {
          ...(this.opts.env || process.env),
        },
        cwd: this.opts.cwd,
      })
    } else {
      const argsToPass = process.platform === 'win32'
        ? ['', SCREENIE_BIN, ...this.args] // for some reason Windows needs this blank first argument???
        : [SCREENIE_BIN, ...this.args]

      const nodePath = process.platform === 'win32'
        ? 'C:\\Program Files\\nodejs\\node.exe'
        : 'node'

      this.term = pty.spawn(nodePath, argsToPass, {
        name: 'xterm-color',
        cols: 130,
        rows: 30,
        cwd: this.opts.cwd,
        env: {
          ...(this.opts.env || process.env),
        },
      })
    }

    if (this.opts.tty === false) {
      this.term.stdout.pipe(process.stdout)
      this.term.stderr.pipe(process.stderr)

      this.term.stdout.on('data', data => this.output += data.toString())
      this.term.stderr.on('data', data => this.output += data.toString())
    } else {
      this.term.pipe(process.stdout)

      this.term.on('data', data => this.output += data.toString())
    }

    this.term.on('exit', () => {
      console.log()
      console.log(' - Exited')
      console.log()

      this.exited = true
    })
  }

  async send(keyCode) {
    // synthetic delay
    await new Promise(resolve => setTimeout(resolve, SYNTHETIC_DELAY_MS))

    this.term.write(keyCodes[keyCode] || keyCode)
  }

  waitForDone() {
    if (this.exited) {
      return stripAnsi(this.output)
    }

    return new Promise(resolve =>
      this.term.once('exit', () => resolve(stripAnsi(this.output)))
    )
  }

  waitForLaunch() {
    if (this.opts.tty === false) {
      return new Promise((resolve, reject) => {
        this.term.stdout.once('data', resolve)
      })
    } else {
      return new Promise((resolve, reject) => {
        this.term.once('data', resolve)
      })
    }
  }

  async waitForMatch(stringMatch) {
    if (this.output.match(stringMatch)) return true

    await new Promise(resolve => {
      this.term.on('data', () => {
        if (this.output.match(stringMatch)) {
          resolve(true)
        }
      })
    })
  }

  getOutput() {
    return stripAnsi(this.output)
  }

  getRawOutput() {
    return this.output
  }

  kill() {
    this.term.kill()
  }

}
