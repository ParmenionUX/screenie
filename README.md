Screenie
========

> Lightweight regression test runner designed for capturing and comparing screenshots

Screenie is a lightweight regression test runner designed for capturing and comparing screenshots.
It is inspired from this excellent talk by Brett Slatkin (https://www.youtube.com/watch?v=1wHr-O6gEfc).

How you capture your screenshots is up to you, but out of the box it supports Webdriver and uses Docker.

Getting started
---------------

This example uses `screenie-webdriver` for capturing webpages and `screenie-webdriver-docker` to quickly get a Selenium environment set up. Make sure you have Node >8, Docker and Docker Compose installed before continuing.

Install the relevant packages:

```sh
$ npm install --save-dev screenie-core screenie-cli screenie-webdriver screenie-webdriver-docker
```

Create a `tests/regression.js` file:

```js
const { test } = require('screenie-cli')
const { webdriverConnectionDetails } = require('screenie-webdriver-docker')
const { createWebdriver } = require('screenie-webdriver')

const { testRoute } = createWebdriver({
  config: matrix =>
    Object.assign({}, webdriverConnectionDetails, {
      desiredCapabilities: {
        browserName: 'chrome',
      },
      baseUrl: 'http://localhost:8080',
    }),
})

test('layout', testRoute('/layout', opts))
test('typography', testRoute('/typography', opts))
```

*Make sure there is something at localhost:8080/layout before continuing, or just update the routes above*

Spin up your Selenium stack:

```sh
$ eval "$(./node_modules/screenie-webdriver-docker/env)"
$ docker-compose up -d --scale chrome=5 # use 5 chrome instances in parallel
```

Run the tests:

```sh
# You can use a glob if you want
$ npx screenie tests/regression.js
```

Enjoy. You can see a more complete example in the `examples` folder.


Reference screenshots and differences
----------------------------

The first time the tests run, Screenie will save the screenshots in a `__screenshots__` folder next to the test files. These are referred to as the "reference screenshots" (also known as baseline screenshots).

Subsequent runs of Screenie will compare the current screenshots captured with the reference screenshots in source control.

When running in an interactive terminal on your development machine, Screenie will offer to open up a browser window which will display all of the differences, and you'll have the option to update the reference screenshot if you've reviewed the diff and it's a valid change.

If Screenie is running on CI or a machine without an interactive terminal, it will fail if there are any differences between screenshots.

This mechanism is similar to how Jest Snapshots work.

If there is no interactive terminal available, all of the diffs will be recorded and outputted in HTML in the `diff_output` folder in the current working directory. If you're running Screenie on a CI server, you could then save the `diff_output` folder as an artifact so that it can be reviewed after a build. In the future, an optional HTML report will be output here as well.


Interactive mode
----------------

If you're actively working on your site / components, you can run Screenie in "interactive mode" which doesn't run any tests by default, but spins up a browser where you can choose which tests to run and have the option to rerun them as you make changes.

Start Screenie in interactive mode by passing the `--interactive` flag:

```sh
$ ./node_modules/.bin/screenie --interactive tests/regression.js
```


Using Babel
-----------

You can use Babel or any other transpiler with a require hook by running Screenie like so:

```sh
$ ./node_modules/.bin/screenie -r babel-register -r babel-polyfill tests/regression.js
```

Make sure you have a `.babelrc` file present and have installed all of the necessary Babel packages.


License
-------

Licensed under the MIT License.

View the full license [here](https://raw.githubusercontent.com/ParmenionUX/screenie/master/LICENSE).
